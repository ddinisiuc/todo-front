import { createWebHistory, createRouter } from "vue-router";

import Board from '/src/views/Board.vue';
import App from "@/App.vue";

const routes = [
    // { path: '/', name: 'home', redirect: '/'},
    {
        path: '/board/:slug',
        name: 'board',
        component: Board}
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
import axios from "axios";

const axiosInstance = axios.create({
    // Set the base URL for your API requests
    baseURL: 'http://localhost:8000/v1',

    // Enable cross-domain requests
    crossDomain: true,
    // headers: {
    //     'Access-Control-Allow-Origin': '*',
    //     'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
    //     'Access-Control-Allow-Headers': 'Content-Type'
    // }
});

const state = {
    boards: [],
    selectedBoard: {},
    tasks: []
};

const mutations = {
    setBoards(state, boards) {
        state.boards = boards;
    },

    setBoard(state, board) {
        state.selectedBoard = board
    },
    setBoardTasks(state, tasks) {
        state.tasks = tasks
    }
};

const actions = {
     async fetchBoards({ commit }) {
         try {
             const res = await axiosInstance.get('/boards');
             const data = res.data;
             commit('setBoards', data.data.boards);
         } catch (error) {
             console.error('Error fetching boards:', error);
             // commit('SAVE_RESPONSE_STATE', {
             //     type: 'boards',
             //     currState: false,
             // });
         }
    },

    async fetchBoard({ commit }, {boardSlug}) {
        try {
            console.log(boardSlug)

            const res = await axiosInstance.get('/boards/' + boardSlug);
            const data = res.data;
            const { tasks, ...board } = data.data.board

            commit('setBoard', board);
            commit('setBoardTasks', tasks)

        } catch (error) {
            console.error('Error fetching boards:', error);
            commit('SAVE_RESPONSE_STATE', {
                type: 'boards',
                currState: false,
            });
        }
    },

    async updateTaskStatus({ commit }, { boardId, task, taskStatus }) {
        try {
            const res = await axiosInstance.put('/boards/' + boardId + '/todos/' + task.id, {
                status: taskStatus,
            });
            const data = res.data;
            // Assuming there's an API endpoint to update the task status
            const updatedTask = { ...task, status: taskStatus };
            console.log(updatedTask)
            // Update task status in the Vuex store
            const updatedTasks = state.tasks.map(t => t.id === updatedTask.id ? updatedTask : t);

            console.log(updatedTasks)
            commit('setBoardTasks', updatedTasks);
        } catch (error) {
            console.error('Failed to update task status:', error);
        }
    }
};

const getters = {
    getBoards: state => state.boards,
    getCurrentBoard: state => state.selectedBoard,
    getBoardTasks: state => state.tasks
};

export default {
    state,
    mutations,
    actions,
    getters,
};

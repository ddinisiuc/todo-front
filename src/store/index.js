import { createStore } from "vuex";
import board from './modules/board'
// import todo from './modules/todo'
export default  createStore({
    modules: {
        board,
        // todo
    },
})
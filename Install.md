# Project installation

### Requirements
- Node js v18.18.1
- npm  v9.0.1

### Local
For installation all dependencies and libraries, run
```
npm install
```
To start localhost server run command:
 ```
vite
 ```